#include "SubClass.hpp"
//#include <stdexcept>

namespace Foo{

  void SubClass::exec(){
    y = rInt10();
    changeX20();
  }
  
  int SubClass::rIntx3(int a){
    //throw std::runtime_error("hoge");
    return a *3;
  }

  int SubClass::rInt10(){
    return 10;
  }

  void SubClass::changeX20(){
    x = 20;
  }
}