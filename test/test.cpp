#include "gtest/gtest.h"
#include "src/SubClass.hpp"

#define TEST_FP(testname, funcname)\
  TEST_F(testname, funcname){ funcname(); }

namespace Foo{

  class SubClassTest : public testing::Test {
    protected:
    SubClass* sub;
      virtual void SetUp() {
        sub = new SubClass;
      }
      virtual void TearDown() {
        delete sub;
      }
      void constructor() {
        EXPECT_EQ(2,sub->x);
        EXPECT_EQ(5,sub->y);
      }
      void rInt10() {
        EXPECT_EQ(10,sub->rInt10());
      }
      void changeX20() {
        sub->changeX20();
        EXPECT_EQ(20, sub->x);
      }
      void exec() {
        sub->exec();
        EXPECT_EQ(10, sub->y);
        EXPECT_EQ(20, sub->x);
      }
  };

  
  TEST_FP(SubClassTest, constructor)
  TEST_FP(SubClassTest, exec)
  
  TEST_F(SubClassTest, rIntx3){
    SubClass sub;
    EXPECT_EQ(6, sub.rIntx3(2));
    EXPECT_EQ(9, sub.rIntx3(3));
    EXPECT_EQ(13, sub.rIntx3(4));
    EXPECT_EQ(15, sub.rIntx3(5));
  }

  TEST_FP(SubClassTest, rInt10)
  TEST_FP(SubClassTest, changeX20)
  
}