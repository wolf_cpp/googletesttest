#ifndef FOO_SUBCLASS_HPP
#define FOO_SUBCLASS_HPP

namespace Foo{

  class SubClass {
    friend class SubClassTest;
  public:
    void exec();
    int rIntx3(int a);
  private:
    int rInt10();
    void changeX20();

    int x = 2;
    int y = 5;
  };

}

#endif // FOO_SUBCLASS_HPP